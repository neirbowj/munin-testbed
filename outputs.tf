output "r8_username" {
  description = "Default username on R8 AMI"
  value       = var.r8_username
}

output "r9_username" {
  description = "Default username on R9 AMI"
  value       = var.r9_username
}

output "r8_master_id" {
  description = "ID of Rocky Linux 8 instance in Munin master role"
  value       = aws_instance.r8_master.id
}

output "r8_master_fqdn" {
  description = "Public domain name of RHEL 8 instance in Munin master role"
  value       = aws_instance.r8_master.public_dns
}

output "r9_master_id" {
  description = "ID of Rocky Linux 9 instance in Munin master role"
  value       = aws_instance.r9_master.id
}

output "r9_master_fqdn" {
  description = "Public domain name of RHEL 9 instance in Munin master role"
  value       = aws_instance.r9_master.public_dns
}

output "r8_node_id" {
  description = "ID of Rocky Linux 8 instance in Munin node role"
  value       = aws_instance.r8_node.id
}

output "r8_node_fqdn" {
  description = "Public domain name of RHEL 8 instance in Munin node role"
  value       = aws_instance.r8_node.public_dns
}

output "r9_node_id" {
  description = "ID of Rocky Linux 9 instance in Munin node role"
  value       = aws_instance.r9_node.id
}

output "r9_node_fqdn" {
  description = "Public domain name of RHEL 9 instance in Munin node role"
  value       = aws_instance.r9_node.public_dns
}

