variable "key_pair_name" {
  description = "SSH key pair name for EC2 instances"
  type        = string
}

variable "r8_ami_id" {
  description = "RedHat 8 or derivative AMI ID"
  type        = string
  # Rocky-8-EC2-Base-8.8-20230518.0.x86_64
  default = "ami-03cf1b432c549f1b4"
}

variable "r8_username" {
  description = "EC2 default username for R8 AMI"
  type        = string
  default     = "rocky"
}

variable "r9_ami_id" {
  description = "RedHat 9 or derivative AMI ID"
  type        = string
  # Rocky-9-EC2-Base-9.2-20230513.0.x86_64
  default = "ami-03d39810179b00672"
}

variable "r9_username" {
  description = "EC2 default username for R9 AMI"
  type        = string
  default     = "rocky"
}

variable "extra_default_tags" {
  description = "Extra tags to be applied to all AWS resources"
  type        = map(string)
  default     = {}
}
