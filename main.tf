provider "aws" {
  region = "us-east-2"
  default_tags {
    tags = merge({
      Project = "Munin / OpenSSL testbed"
      Repo    = "neirbowj/munin-testbed"
    }, var.extra_default_tags)
  }
}

data "aws_vpc" "default" {
  default = true
}

resource "aws_security_group" "munin_hosts" {
}

resource "aws_vpc_security_group_ingress_rule" "in_allow_munin_self" {
  security_group_id = aws_security_group.munin_hosts.id

  description                  = "Allow TCP/4949 from own SG"
  referenced_security_group_id = aws_security_group.munin_hosts.id
  ip_protocol                  = "tcp"
  from_port                    = 4949
  to_port                      = 4949
}

resource "aws_vpc_security_group_egress_rule" "out_allow_all_ipv4" {
  security_group_id = aws_security_group.munin_hosts.id

  description = "Allow all IPv4"
  cidr_ipv4   = "0.0.0.0/0"
  ip_protocol = -1
}

resource "aws_vpc_security_group_egress_rule" "out_allow_all_ipv6" {
  security_group_id = aws_security_group.munin_hosts.id

  description = "Allow all IPv6"
  cidr_ipv6   = "::/0"
  ip_protocol = -1
}

resource "aws_vpc_security_group_ingress_rule" "in_allow_ssh_ipv4" {
  security_group_id = aws_security_group.munin_hosts.id

  description = "Allow SSH via IPv4"
  cidr_ipv4   = "0.0.0.0/0"
  ip_protocol = "tcp"
  from_port   = 22
  to_port     = 22
}

resource "aws_vpc_security_group_ingress_rule" "in_allow_ssh_ipv6" {
  security_group_id = aws_security_group.munin_hosts.id

  description = "Allow SSH via IPv6"
  cidr_ipv6   = "::/0"
  ip_protocol = "tcp"
  from_port   = 22
  to_port     = 22
}

resource "aws_instance" "r8_master" {
  ami           = var.r8_ami_id
  instance_type = "t3.small"
  key_name      = var.key_pair_name
  tags = {
    Name = "r8_master"
  }
  vpc_security_group_ids = [
    aws_security_group.munin_hosts.id
  ]
}

resource "aws_instance" "r9_master" {
  ami           = var.r9_ami_id
  instance_type = "t3.small"
  key_name      = var.key_pair_name
  tags = {
    Name = "r9_master"
  }
  vpc_security_group_ids = [
    aws_security_group.munin_hosts.id
  ]
}

resource "aws_instance" "r8_node" {
  ami           = var.r8_ami_id
  instance_type = "t3.small"
  key_name      = var.key_pair_name
  tags = {
    Name = "r8_node"
  }
  vpc_security_group_ids = [
    aws_security_group.munin_hosts.id
  ]
}

resource "aws_instance" "r9_node" {
  ami           = var.r9_ami_id
  instance_type = "t3.small"
  key_name      = var.key_pair_name
  tags = {
    Name = "r9_node"
  }
  vpc_security_group_ids = [
    aws_security_group.munin_hosts.id
  ]
}

resource "local_file" "ansible_inventory" {
  filename        = "ansible/inventory.yml"
  file_permission = "0640"
  content = templatefile("ansible/inventory.yml.tmpl",
    {
      ansible_master_hosts = [
        {
          alias       = "r8_master"
          public_dns  = aws_instance.r8_master.public_dns
          private_dns = aws_instance.r8_master.private_dns
          username    = var.r8_username
        },
        {
          alias       = "r9_master"
          public_dns  = aws_instance.r9_master.public_dns
          private_dns = aws_instance.r9_master.private_dns
          username    = var.r9_username
        }
      ]
      ansible_node_hosts = [
        {
          alias       = "r8_node"
          public_dns  = aws_instance.r8_node.public_dns
          private_dns = aws_instance.r8_node.private_dns
          username    = var.r8_username
        },
        {
          alias       = "r9_node"
          public_dns  = aws_instance.r9_node.public_dns
          private_dns = aws_instance.r9_node.private_dns
          username    = var.r9_username
        }
      ]
    }
  )
}
