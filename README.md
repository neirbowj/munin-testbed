# Munin TLS Testbed

## Summary

This repository contains a Terraform project and supporting Ansible
playbook intended to demonstrate a working (or maybe not) TLS
configuration using AWS EC2 instances running some flavor of Red Hat
Enterprise Linux 8 and 9.

Only the polling component of Munin is implemented, not the web
portal.

## Requirements

Aside from having working installs of Terraform and Ansible on your
machine, this project expects you to:

* supply AWS CLI credentials via one of the implicit mechanisms (e.g.
  environment variables and/or the `~/.aws/credentials` file)
* have a default VPC in the `us-east-2` region into which you can
  usefully deploy EC2 instances
* supply the name of an SSH key pair in that region as a TF var

## Infrastructure

The Terraform project launches four instances, two RHEL 8 flavored and
two RHEL 9 flavored (default: Rocky Linux).

The Ansible playbooks configure all four as Munin Nodes and one of
each as Munin Masters, issuing end-entity certificates from a private
CA all around.

## Varying the certificate message digest algorithm

By default, the private root CA certificate will use SHA1, and each
end entity (EE) certificate will use SHA256. These can be overridden
on the CLI when running the Ansible playbook, either catagorically or,
for the EE's, by individual host alias.

If the key for any certificate already exists in the `ansible/files`
directory, Ansible will not attempt to generate that key/cert pair. To
re-generate a certificate, delete its key locally. Re-generating the
root CA certificate will automatically cause all EE certificates to be
re-generated.

See a list of the valid values prefixed by "-" by running:

    openssl dgst -list

Only "sha1" and "sha256" have been tested.

### Examples

To use SHA-256 instead of SHA-1 for the root CA cert:

    ansible-playbook -e 'root_md=sha256' play.yml

To use SHA-1 instead of SHA-256 for all EE certs:

    ansible-playbook -e 'default_ee_md=sha1' play.yml

To use SHA-1 for only the R9 Master EE cert:

    ansible-playbook -e '{"ee_md":{"r9_master":"sha1"}}' play.yml

To use SHA-256 for the root CA cert and SHA-1 for the R9 Master EE cert:

    ansible-playbook \
        -e '{"root_md":"sha256","ee_md":{"r9_master":"sha1"}}' \
        play.yml

Or, if you happen to have `jo` available:

    ansible-playbook \
        -e $(jo root_md=sha256 ee_md=$(jo r9_master=sha1)) \
        play.yml

## Injecting additional tags

If you would like to override or extend `default_tags.tags` for the AWS
provider, use the `extra_default_tags` variable. For example, create a
`terraform.tfvars` file containing:

    extra_default_tags = {
      ManagementNode = "dumbledore"
      ExperimentID   = "555-1212/X"
    }

## Enable monitoring Master hosts

By default, each Master host will only poll both Node hosts. To poll
all Master hosts too, set `enable_monitor_masters` to a true value,
either globally or per-Master host.

## Varying the system-wide crypto policies

By default, each host will remain set to the natural default of
`DEFAULT`. This can be overridden on the CLI when running the Ansible
playbook, either catagorically or by individual host alias. The
`crypto_policies` role makes no attempt to validate whether a specified
value is supported on a particular host.

### Examples

To use `FUTURE` for all hosts:

    ansible-playbook -e 'crypto_policies_default=FUTURE'

To use `DEFAULT:SHA1` on the R9 Master host:

    ansible-playbook \
        -e '{"crypto_policies":{"r9_master":"DEFAULT:SHA1"}}' \
        play.yml
